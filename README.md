# Openlink docker

## Installation


```commandline
cp .env.sample .env
```
Then complete .env file following documentation for each api and frontend.

### VAULT
Then change authentication parameter at the begining of `vault_configurator/vault_config.sh` to match your authentication provider.
```shell
## vault_configurator/vault_config.sh ##

export VAULT_ADDR="http://vault:8200"
oidc_discovery_url=""
bound_issuer=""
bound_audiences=""
```
### BUILD and RUN
To finish build and run with docker-compose. If api and client are already running just run vault with docker-compose.
```commandline
sudo docker-compose build
sudo docker-compose up
```
> :warning: **Make sure to grab unseal key**: If you lost them you will have to reset your vault to do so delete `vault/data` folder.