#!/bin/bash

export VAULT_ADDR="http://vault:8200"
oidc_discovery_url="https://iam.mesocentre.uca.fr/"
bound_issuer="https://iam.mesocentre.uca.fr/"
bound_audiences="2681ecf0-4c72-4087-817d-6c71520b27fb"

initialized=$(curl ${VAULT_ADDR}/v1/sys/seal-status | jq -r ".initialized")

if [ "$initialized" == "true" ]; then
    echo "Vault already configured, unseal it with <vault operator unseal> if needed"
    exit 1
else
    echo "Configure vault"
fi

# Parse unsealed keys and root
curl --request POST --data '{"secret_shares": 5, "secret_threshold": 3}' ${VAULT_ADDR}/v1/sys/init > init.json

keys=$(cat init.json | jq -r ".keys")
readarray -t key_list < <(jq -r '.[]' <<<"$keys")

root_token=$(cat init.json | jq -r ".root_token")

# Unseal vault
curl --request POST --data '{"key":"'${key_list[0]}'"}' ${VAULT_ADDR}/v1/sys/unseal
curl --request POST --data '{"key":"'${key_list[1]}'"}' ${VAULT_ADDR}/v1/sys/unseal
curl --request POST --data '{"key":"'${key_list[2]}'"}' ${VAULT_ADDR}/v1/sys/unseal

# Enable openlink kv path
curl --header 'X-Vault-Token: '$root_token'' --request POST --data '{"type": "kv","options": {"version": "1"}}' ${VAULT_ADDR}/v1/sys/mounts/openlink

# Set openlink_user policy
curl --header 'X-Vault-Token: '$root_token'' --request POST --data '{"policy": "path \"openlink/{{identity.entity.id}}/*\" {  capabilities = [\"create\", \"update\", \"read\", \"delete\", \"list\"]}path \"openlink/{{identity.entity.id}}\" {  capabilities = [\"create\", \"update\", \"read\", \"delete\", \"list\"]}path \"openlink/shared/{{identity.entity.id}}/*\" {  capabilities = [\"create\", \"update\", \"read\", \"delete\", \"list\"]}path \"openlink/shared/{{identity.entity.id}}\" {  capabilities = [\"create\", \"update\", \"read\", \"delete\", \"list\"]}"}' ${VAULT_ADDR}/v1/sys/policy/openlink_user

# Enable jwt
curl --header 'X-Vault-Token: '$root_token'' --request POST --data '{"type": "jwt"}' ${VAULT_ADDR}/v1/sys/auth/jwt
curl --header 'X-Vault-Token: '$root_token'' --request POST --data '{"oidc_discovery_url": "'$oidc_discovery_url'", "bound_issuer": "'$bound_issuer'", "oidc_client_id": "", "oidc_client_secret": ""}' ${VAULT_ADDR}/v1/auth/jwt/config
curl --header 'X-Vault-Token: '$root_token'' --request POST --data '{"policies": "openlink_user", "user_claim": "sub", "role_type": "jwt", "bound_subject": "", "bound_audiences": "'$bound_audiences'"}' ${VAULT_ADDR}/v1/auth/jwt/role/openlink_user

cat init.json
rm init.json