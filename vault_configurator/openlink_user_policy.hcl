path "openlink/{{identity.entity.id}}/*" {
  capabilities = ["create", "update", "read", "delete", "list"]
}
path "openlink/{{identity.entity.id}}" {
  capabilities = ["create", "update", "read", "delete", "list"]
}
path "openlink/shared/{{identity.entity.id}}/*" {
  capabilities = ["create", "update", "read", "delete", "list"]
}
path "openlink/shared/{{identity.entity.id}}" {
  capabilities = ["create", "update", "read", "delete", "list"]
}